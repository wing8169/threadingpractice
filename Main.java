import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

class Position {
    private int x, y;

    public Position(int x, int y){
        this.x = x;
        this.y = y;
    }

    public void setX(int x){this.x = x;}
    public int getX(){return x;}
    public void setY(int y){this.y = y;}
    public int getY(){return y;}
}

class SnowFall implements Runnable {
    
    private volatile boolean isRunning = true;
    private Position positionController;
    private ArrayList<Position> snows = new ArrayList<>();


    public SnowFall(Position positionController){
        this.positionController = positionController;
    }

    public void shutDown(){
        isRunning = false;
    }

    @Override
    public void run(){
        while(isRunning){
            try {
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            } catch(Exception e) {

            }

            snows.add(new Position(positionController.getX(), 0));
            
            int snowNum = snows.size();
            for(int i=snowNum-1; i>=0; i--) {
                if(snows.get(i).getY() > 15) {
                    snows.remove(i);
                    continue;
                }
                snows.get(i).setY(snows.get(i).getY()+1);

                for(int j=0; j<snows.get(i).getX(); j++) {
                    System.out.print(" ");
                }
                System.out.println("*");
            }

            try {
                Thread.sleep(1000);
            } catch(Exception e) {

            }
        }     
    }

}

public class Main {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Position positionController = new Position(0, 0);
        SnowFall snowFall = new SnowFall(positionController);
        Thread thread = new Thread(snowFall);
        thread.start();
        while(true) {
            int snowPosition = Integer.parseInt(br.readLine());
            if(snowPosition == -1) {
                snowFall.shutDown();
                break;
            }
            positionController.setX(snowPosition);
        }
    }
}